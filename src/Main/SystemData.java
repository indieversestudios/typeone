package Main;

public class SystemData {

	public static final String PATH_TO_BLOCK_TEXTURES = "/textures/world_blocks.png";
	public static final int BLOCK_WIDTH = 64;
	public static final int BLOCK_HEIGHT = 64;
	
}

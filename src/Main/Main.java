package Main;

import java.awt.event.KeyEvent;

import javax.swing.JFrame;

import Player.LocalPlayer;
import Screens.GameScreen;
import World.World;
import de.Major_Sauce.Game.Game;
import de.Major_Sauce.Game.InputManager.Key;
import de.Major_Sauce.IndieEngine.IndieEngine;

public class Main extends Game {
	private static final long serialVersionUID = 1L;
	
	public static void main(String[] args) {
		IndieEngine engine = new IndieEngine();
		engine.start(new Main());
	}
	
	private World world;
	private GameScreen screen;
	public LocalPlayer p;
	public int playerId;
	
	@Override
	public void preInit() {
		//Debugging
		systemVars.debuggFrameLimit = false;
		systemVars.debuggUpdateLimit = true;
		systemVars.debuggMode = true;
		
		//Inits
		systemVars.name = "TypeOne";
		systemVars.version = "0.0.4 Floor changing :D";
		systemVars.preferredFPS = 60;
		systemVars.preferredUPS = 60;
		systemVars.buffers = 2;
		systemVars.defaultCloseOperation = JFrame.EXIT_ON_CLOSE;
		systemVars.width = 1600;
		systemVars.height = 900;
		systemVars.resizable = false;
		systemVars.showFps = true;
		systemVars.transparentColor = -65316;
	}
	
	@Override
	public void afterInit() {
		
		screen = new GameScreen(screenManager, systemVars.transparentColor, systemVars.width, systemVars.height);
		world = new World(this, 64, 64);
		p = new LocalPlayer(playerId, world, inputManager);
		p.setWorld(world);
		action1 = inputManager.createKey(KeyEvent.VK_SPACE);
	}

	private Key action1;
	
	private int actionCooldown = 0;
	
	@Override
	public void update(int sinceLastUpdate) {
		p.update(sinceLastUpdate);
		if(actionCooldown == 0 && action1.isPressed()){
			actionCooldown += 10;
		}
		if(actionCooldown > 0){
			actionCooldown--;
		}
	}
	
	@Override
	public void render(int[] pixels) {
		screen.wipe();
		world.render(screen);
		screen.render(pixels);
	}
	
	//GETTERS AND SETTERS
	public World getWorld(){
		return world;
	}
	
	public LocalPlayer getLocalPlayer(){
		return p;
	}
	
}

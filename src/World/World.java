package World;

import java.util.ArrayList;

import de.Major_Sauce.Game.Game;
import de.Major_Sauce.Game.ScreenManager.Screen;
import Block.Block;
import Block.BlockType;
import Block.CoverType;
import Player.Player;

public class World {

	private ArrayList<Player> players = new ArrayList<Player>();
	
	private int width;
	private int height;
	private Block[] blocks;
	
	private final CoverType defaultCoverType = CoverType.RED_SAND_DRIVEN_COVER;
	
	public World(Game game, int width, int height){
		this.width = width;
		this.height = height;
		blocks = new Block[width * height];
		for(int x = 0; x < width; x++){
			for(int y = 0; y < height; y++){
				int index = x + y * width;
				if(index == 0){
					blocks[index] = new Block(BlockType.FURNANCE, new Location(x, y), defaultCoverType); 			
					continue;
				}
				if(index == 1){
					blocks[index] = new Block(BlockType.RAFFINERY, new Location(x, y), defaultCoverType); 			
					continue;
				}
				if(index == 2){
					blocks[index] = new Block(BlockType.SOLAR_PANEL, new Location(x, y), defaultCoverType); 			
					continue;
				}
				if(Math.random() > 0.2){
					blocks[index] = new Block(BlockType.DIRT, new Location(x, y), defaultCoverType); 			
				} else if(Math.random() > 0.1){
					blocks[index] = new Block(BlockType.STONE, new Location(x, y), defaultCoverType); 			
				} else if(Math.random() > 0.05){
					blocks[index] = new Block(BlockType.GRANITE, new Location(x, y), defaultCoverType); 			
				} else {
					blocks[index] = new Block(BlockType.SANDSTONE, new Location(x, y), defaultCoverType); 			
				}
			}
		}	
	}
	
	public void addPlayer(Player p){
		if(!players.contains(p)){
			players.add(p);
		}
	}
	
	public void removePlayer(Player p){
		if(players.contains(p)){
			players.remove(p);
		}
	}
	
	public void tick(){
		for(Block b : blocks){
			b.tick();
		}
	}
	
	public void render(Screen screen){
		for(int x = 0; x < width; x++){
			for(int y = 0; y < height; y++){
				Block b = blocks[x + y * width];
				b.render(screen);
			}
		}
		for(Player p : players){
			p.render(screen);
		}
	}
	
	public Player getPlayer(int playerId){
		for(Player p : players){
			if(p.getPlayerId() == playerId){
				return p;
			}
		}
		return null;
	}
	
	public Block getBlockAt(int x, int y){
		return blocks[x + y * width];
	}
	
}

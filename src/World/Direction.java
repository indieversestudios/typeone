package World;

import de.Major_Sauce.Units.Vector;

public enum Direction {

	NONE(0, 0), NORTH(0, -1), EAST(1, 0), SOUTH(0, 1), WEST(-1, 0);
	
	private int xMod;
	private int yMod;
	
	private Direction(int xMod, int yMod){
		this.xMod = xMod;
		this.yMod = yMod;
	}
	
	public int getXMod(){
		return xMod;
	}
	
	public int getYMod(){
		return yMod;
	}
	
	public Vector getModsAsVelocity(){
		return new Vector(xMod, yMod);
	}
	
	public static String toString(Direction dir){
		switch (dir) {
		case NORTH:
			return "NORTH";
		case EAST:
			return "EAST";
		case SOUTH:
			return "SOUTH";
		case WEST:
			return "WEST";
		case NONE:
			return "NONE";	
		default:
			return null;
		}
	}
	
	public static Direction parseDir(String direction){
		switch (direction.toUpperCase()) {
		case "NORTH":
			return NORTH;
		case "EAST":
			return EAST;
		case "SOUTH":
			return SOUTH;
		case "WEST":
			return WEST;
		case "NONE" : {
			return NONE;
		}
		default:
			return null;
		}
	}
	
	public static Direction toDirection(double x, double y){
		if(x == 0 && y == 0 || x == y){
			return NONE;
		}
		if(Math.abs(x) > Math.abs(y)){
			if(x > 0){
				return EAST;
			} else {
				return WEST;
			}
		} else {
			if(y > 0){
				return SOUTH;
			} else {
				return NORTH;
			}
		}
	}
	
}

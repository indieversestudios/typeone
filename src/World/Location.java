package World;

import Main.SystemData;
import de.Major_Sauce.Units.Point;

public class Location extends Point {

	public Location(){
		setX(0);
		setY(0);
	}
	
	public Location(int x, int y) {
		setX(x);
		setY(y);
	}

	public int toPixelPosX(){
		return (int)(getX() * SystemData.BLOCK_WIDTH);
	}
	
	public int toPixelPosY(){
		return (int)(getY() * SystemData.BLOCK_HEIGHT);
	}
	
}

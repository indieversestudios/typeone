package Entity;

import World.Location;
import World.World;
import de.Major_Sauce.Game.ScreenManager.Screen;
import de.Major_Sauce.Units.Vector;

public class Entity {

	private Location location;
	private Vector velocity;
	private EntityType type;
	private World world;
	
	public Entity(EntityType type, World world){
		this.type = type;
		location = new Location();
		velocity = new Vector();
	}
	
	public void update(int sinceLastUpdate){
	}
	
	public void render(Screen screen){
	}
	
	public EntityType getType(){
		return type;
	}
	
	public Location getLocation(){
		return location;
	}
	
	public World getWorld(){
		return world;
	}
	
	public void setWorld(World world){
		this.world = world;
	}
	
	public Vector getVelocity(){
		return velocity;
	}
	
}

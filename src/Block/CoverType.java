package Block;

import Main.Main;
import Main.SystemData;
import de.Major_Sauce.Graphics.SpriteSheet;
import de.Major_Sauce.Graphics.Tile;

public enum CoverType {

	RED_SAND_FULL_COVER(8), RED_SAND_DRIVEN_COVER(7);
		
	private static SpriteSheet sheet = Main.spriteSheetManager.loadSpriteSheet(SystemData.PATH_TO_BLOCK_TEXTURES, SystemData.BLOCK_WIDTH, SystemData.BLOCK_HEIGHT);
		
	private Tile texture;
		
	private CoverType(int tileNum){
		texture = getTile(tileNum);
	}
	
	private Tile getTile(int num){
		if(sheet == null){
			loadSpriteSheet();
		}
		return sheet.getTile(num);
	}
	
	public Tile getTexture(){
		return texture;
	}
	
	private static void loadSpriteSheet(){
		sheet = Main.spriteSheetManager.loadSpriteSheet(SystemData.PATH_TO_BLOCK_TEXTURES, SystemData.BLOCK_WIDTH, SystemData.BLOCK_HEIGHT);	
		System.out.println("Loaded coverType sheet");
	}
		
}

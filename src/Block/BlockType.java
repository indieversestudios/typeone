package Block;

import de.Major_Sauce.Graphics.SpriteSheet;
import de.Major_Sauce.Graphics.Tile;
import Main.Main;
import Main.SystemData;

public enum BlockType {

	STONE(0), GRANITE(1), DIRT(2), SANDSTONE(3), FURNANCE(4), RAFFINERY(5), SOLAR_PANEL(6);
	
	private static SpriteSheet sheet = Main.spriteSheetManager.loadSpriteSheet(SystemData.PATH_TO_BLOCK_TEXTURES, SystemData.BLOCK_WIDTH, SystemData.BLOCK_HEIGHT);
	
	private Tile texture;
	
	private BlockType(int tileNum){
		texture = getTile(tileNum);
	}
	
	private Tile getTile(int num){
		if(sheet == null){
			loadSpriteSheet();
		}
		return sheet.getTile(num);
	}
	
	public Tile getTexture(){
		return texture;
	}
	
	private static void loadSpriteSheet(){
		sheet = Main.spriteSheetManager.loadSpriteSheet(SystemData.PATH_TO_BLOCK_TEXTURES, SystemData.BLOCK_WIDTH, SystemData.BLOCK_HEIGHT);	
		System.out.println("Loaded blockType sheet");
	}
	
}

package Block;

import de.Major_Sauce.Game.ScreenManager.Screen;
import World.Location;

public class Block {

	private BlockType blockType;
	private Location location;
	private CoverType coverType;
	
	public Block(BlockType blockType, Location location, CoverType coverType){
		this.blockType = blockType;
		this.location = location;
		this.coverType = coverType;
	}
	
	public Location getLocation(){
		return location;
	}
	
	public BlockType getType(){
		return blockType;
	}
	
	public void setBlockType(BlockType blockType){
		this.blockType = blockType;
	}
	
	public CoverType getCoverType(){
		return coverType;
	}
	
	public void setCoverType(CoverType coverType){
		this.coverType = coverType;
	}
	
	public void tick(){
	}
	
	public void render(Screen screen){
		if(coverType == CoverType.RED_SAND_FULL_COVER){
			screen.renderTile(coverType.getTexture(), location.toPixelPosX(), location.toPixelPosY());
		} else {
			screen.renderTile(blockType.getTexture(), location.toPixelPosX(), location.toPixelPosY());
			screen.renderTile(coverType.getTexture(), location.toPixelPosX(), location.toPixelPosY());
		}
	}
	
}

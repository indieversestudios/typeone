package Player;

import World.Direction;
import World.Location;
import World.World;
import de.Major_Sauce.Game.ScreenManager.Screen;
import de.Major_Sauce.Units.Point;
import de.Major_Sauce.Units.Vector;
import Animation.Animation;
import Animation.Animation.Animatable;
import Entity.Entity;
import Entity.EntityType;

public abstract class Player extends Entity implements Animatable {
	
	public class AnimationContainer {
		
		public Animation idleLeftAnimation;
		public Animation idleRightAnimation;
		public Animation idleUpAnimation;
		public Animation idleDownAnimation;
		
		public Animation walkLeftAnimation;
		public Animation walkRightAnimation;
		public Animation walkUpAnimation;
		public Animation walkDownAnimation;
		
	}
	
	private AnimationContainer animContainer = new AnimationContainer();
	private int playerId;
	private Animation currentAnimation;
	private Direction lastDirection;
	
	private Direction moveDir = Direction.NONE;
	private int timeSinceMovementStart = 0;
	private double timeToMoveOneField = 250;
	private Point movementStartLoc = new Location();
	
	public Player(EntityType type, World world){
		super(type, world);
		loadAnimations(animContainer);
		currentAnimation = animContainer.idleDownAnimation;
	}
	
	public void setPlayerId(int playerId){
		this.playerId = playerId;
	}
	
	public abstract void loadAnimations(AnimationContainer animContainer);
	public abstract void onUpdate(int sinceLastUpdate);
	public abstract void onRender(Screen screen);
	public abstract void onMovementFinish();
	
//	UPDATES
	
	@Override
	public void update(int sinceLastUpdate) {
		currentAnimation.update(sinceLastUpdate);
		onUpdate(sinceLastUpdate);
		onWalk(sinceLastUpdate);
	}
	
	@Override
	public void onAnimationFinish(Animation animation) {
		if(animation == animContainer.idleUpAnimation || animation == animContainer.idleDownAnimation || animation == animContainer.idleLeftAnimation || animation == animContainer.idleRightAnimation){
			animation.start();
		}
	}
	
//	RENDERING STUFF
	
	@Override
	public void render(Screen screen) {
		onRender(screen);
		if(currentAnimation == null){
			System.out.println("curr anim is null");
			return;
		}
		currentAnimation.render(screen, getLocation().toPixelPosX(), getLocation().toPixelPosY());
	}
	
//	MOVEMENT
	
	//needs to be changed later, need to add collision and proper animation handling
	//gonna leave this like that for now...
	public void move(Vector velocity){
		getLocation().move(velocity);
	}
	
	public void onWalk(int sinceLastUpdate){
		if(moveDir == Direction.NONE){
			moveDir = Direction.toDirection(getVelocity().getX(), getVelocity().getY());
			if(moveDir != Direction.NONE){
				movementStartLoc = new Location(getLocation().getXAsInt(), getLocation().getYAsInt());
			} else {
			
			}
		} else {
			timeSinceMovementStart += sinceLastUpdate;
			if(timeSinceMovementStart >= timeToMoveOneField){
				getLocation().set(movementStartLoc.getXAsInt() + moveDir.getXMod(), movementStartLoc.getYAsInt() + moveDir.getYMod());
				moveDir = Direction.NONE;
				timeSinceMovementStart = 0;
				onMovementFinish();
			} else {
				getLocation().set(movementStartLoc.getXAsInt() + (moveDir.getXMod() * (timeSinceMovementStart / timeToMoveOneField)), movementStartLoc.getYAsInt() + moveDir.getYMod() * (timeSinceMovementStart / timeToMoveOneField));
			}
		}
		
		if(moveDir != Direction.NONE){
			lastDirection = moveDir;
		}
		if(moveDir == Direction.NORTH){
			currentAnimation = animContainer.walkUpAnimation;
		} else if(moveDir == Direction.SOUTH){
			currentAnimation = animContainer.walkDownAnimation;
		} else if(moveDir == Direction.EAST){
			currentAnimation = animContainer.walkRightAnimation;
		} else if(moveDir == Direction.WEST){
			currentAnimation = animContainer.walkLeftAnimation;
		} else if(moveDir == Direction.NONE){
			if(lastDirection == Direction.EAST){
				currentAnimation = animContainer.idleRightAnimation;
			} else if(lastDirection == Direction.WEST){
				currentAnimation = animContainer.idleLeftAnimation;
			} else if(lastDirection == Direction.NORTH){
				currentAnimation = animContainer.idleUpAnimation;
			} else if(lastDirection == Direction.SOUTH){
				currentAnimation = animContainer.idleDownAnimation;
			}
		}
		if(!currentAnimation.isRunning()){
			currentAnimation.start();
		}
	}
	
//	GETTERS AND SETTERS
	
	public int getPlayerId(){
		return playerId;
	}
	
	@Override
	public void setWorld(World world) {
		world.removePlayer(this);
		super.setWorld(world);
		world.addPlayer(this);
	}
	
	public AnimationContainer getAnimContainer(){
		return animContainer;
	}
	
	public void setAnimation(Animation anim){
		currentAnimation = anim;
	}
	
}

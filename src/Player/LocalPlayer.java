package Player;

import java.awt.event.KeyEvent;

import de.Major_Sauce.Game.InputManager;
import de.Major_Sauce.Game.InputManager.Key;
import de.Major_Sauce.Game.ScreenManager.Screen;
import de.Major_Sauce.Graphics.SpriteSheet;
import World.World;
import Animation.Animation;
import Animation.Animation.Animatable;
import Block.Block;
import Block.CoverType;
import Entity.EntityType;
import Main.Main;

public class LocalPlayer extends Player implements Animatable {
	
	private Key moveUp;
	private Key moveDown;
	private Key moveLeft;
	private Key moveRight;
	
	public LocalPlayer(int playerId, World world, InputManager inputManager){
		super(EntityType.Player, world);	
		setPlayerId(playerId);
		moveUp = inputManager.createKey(KeyEvent.VK_W);
		moveDown = inputManager.createKey(KeyEvent.VK_S);
		moveLeft = inputManager.createKey(KeyEvent.VK_A);
		moveRight = inputManager.createKey(KeyEvent.VK_D);
	}

	@Override
	public void update(int sinceLastUpdate) {
		super.update(sinceLastUpdate);
	}
	
	@Override
	public void loadAnimations(AnimationContainer animContainer) {
		
		//idleDownAnimation
		animContainer.idleDownAnimation = new Animation(1000 , this) {
			@Override
			protected void load() {
				final SpriteSheet sheet = Main.spriteSheetManager.loadSpriteSheet("/textures/idleDownAnimation.png", 64, 64);
				int scale = 1;
				addTile(sheet.getTile(0, scale), 0, 0, 0);
			}
		};

		//idleLeftAnimation
		animContainer.idleLeftAnimation = new Animation(1000 , this) {
			@Override
			protected void load() {
				final SpriteSheet sheet = Main.spriteSheetManager.loadSpriteSheet("/textures/idleLeftAnimation.png", 64, 64);
				int scale = 1;
				addTile(sheet.getTile(0, scale), 0, 0, 0);
			}
		};

		//idleRightAnimation
		animContainer.idleRightAnimation = new Animation(1000 , this) {
			@Override
			protected void load() {
				final SpriteSheet sheet = Main.spriteSheetManager.loadSpriteSheet("/textures/idleRightAnimation.png", 64, 64);
				int scale = 1;
				addTile(sheet.getTile(0, scale), 0, 0, 0);
			}
		};

		//idleUpAnimation
		animContainer.idleUpAnimation = new Animation(1000 , this) {
			@Override
			protected void load() {
				final SpriteSheet sheet = Main.spriteSheetManager.loadSpriteSheet("/textures/idleUpAnimation.png", 64, 64);
				int scale = 1;
				addTile(sheet.getTile(0, scale), 0, 0, 0);
			}
		};

		//Walk anims
		animContainer.walkDownAnimation = new Animation(800 , this) {
			@Override
			protected void load() {
				final SpriteSheet sheet = Main.spriteSheetManager.loadSpriteSheet("/textures/walkDownAnimation.png", 64, 64);
				int scale = 1;
				addTile(sheet.getTile(0, scale), 0, 0, 0);
				addTile(sheet.getTile(1, scale), 200, 0, 0);
				addTile(sheet.getTile(2, scale), 400, 0, 0);
				addTile(sheet.getTile(3, scale), 600, 0, 0);
			}
		};

		animContainer.walkLeftAnimation = new Animation(800 , this) {
			@Override
			protected void load() {
				final SpriteSheet sheet = Main.spriteSheetManager.loadSpriteSheet("/textures/walkLeftAnimation.png", 64, 64);
				int scale = 1;
				addTile(sheet.getTile(0, scale), 0, 0, 0);
				addTile(sheet.getTile(1, scale), 200, 0, 0);
				addTile(sheet.getTile(2, scale), 400, 0, 0);
				addTile(sheet.getTile(3, scale), 600, 0, 0);
			}
		};

		animContainer.walkRightAnimation = new Animation(800 , this) {
			@Override
			protected void load() {
				final SpriteSheet sheet = Main.spriteSheetManager.loadSpriteSheet("/textures/walkRightAnimation.png", 64, 64);
				int scale = 1;
				addTile(sheet.getTile(0, scale), 0, 0, 0);
				addTile(sheet.getTile(1, scale), 200, 0, 0);
				addTile(sheet.getTile(2, scale), 400, 0, 0);
				addTile(sheet.getTile(3, scale), 600, 0, 0);
			}
		};

		animContainer.walkUpAnimation = new Animation(800 , this) {
			@Override
			protected void load() {
				final SpriteSheet sheet = Main.spriteSheetManager.loadSpriteSheet("/textures/walkUpAnimation.png", 64, 64);
				int scale = 1;
				addTile(sheet.getTile(0, scale), 0, 0, 0);
				addTile(sheet.getTile(1, scale), 200, 0, 0);
				addTile(sheet.getTile(2, scale), 400, 0, 0);
				addTile(sheet.getTile(3, scale), 600, 0, 0);
			}
		};

		
	}

	@Override
	public void onUpdate(int sinceLastUpdate) {
		checkMovement(sinceLastUpdate);
	}

	@Override
	public void onRender(Screen screen) {
		screen.getOffset().setX(getLocation().toPixelPosX() - screen.getWidth() / 2);
		screen.getOffset().setY(getLocation().toPixelPosY() - screen.getHeight() / 2);
	}
	
	private void checkMovement(int sinceLastUpdate){
		int x = 0;
		int y = 0;
		if(moveUp.isPressed()){
			y -= 1;
		}
		if(moveDown.isPressed()){
			y += 1;
		}
		if(moveRight.isPressed()){
			x += 1;
		}
		if(moveLeft.isPressed()){
			x -= 1;
		}
		getVelocity().set(x, y);
	}
	
	@Override
	public void onMovementFinish() {
		Block currentBlock = getWorld().getBlockAt(getLocation().getXAsInt(), getLocation().getYAsInt());
		if(currentBlock.getCoverType() == CoverType.RED_SAND_FULL_COVER){
			currentBlock.setCoverType(CoverType.RED_SAND_DRIVEN_COVER);
		}
	}
	
}

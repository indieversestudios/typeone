package Animation;

import java.util.ArrayList;

import de.Major_Sauce.Game.ScreenManager.Screen;
import de.Major_Sauce.Graphics.Tile;

public abstract class Animation {

	public interface Animatable {
		public void onAnimationFinish(Animation animation);
	}
	
	public class AnimationTile implements Comparable<AnimationTile> {
		
		private Tile tile;
		private int startingTimeMs;
		private int xOffset;
		private int yOffset;
		
		public AnimationTile(Tile tile, int startingTimeMs, int xOffset, int yOffset) {
			this.tile = tile;
			this.startingTimeMs = startingTimeMs;
			this.xOffset = xOffset;
			this.yOffset = yOffset;
		}
		
		public int getStartingTime(){
			return startingTimeMs;
		}
		
		public Tile getTile(){
			return tile;
		}
		
		@Override
		public int compareTo(AnimationTile tile) {
			if(startingTimeMs < tile.startingTimeMs){
				return -1;
			} else if(startingTimeMs > tile.startingTimeMs) {
				return 1;
			}
			return 0;
		}
		
		public int getXOffset(){
			return xOffset;
		}
		
		public int getYOffset(){
			return yOffset;
		}
		
	}
	
	private int totalLengthMs;
	private int currentTileIndex = 0;
	private int currentRuntime = 0;
	private boolean loaded = false;
	private boolean running = false;
	private Animatable animatable;
	
	private AnimationTile currentTile;
	private ArrayList<AnimationTile> animTilesLoadList = new ArrayList<AnimationTile>();
	private AnimationTile[] animTiles;
	
	public Animation(int totalLenghtMs, Animatable animatable){
		this.totalLengthMs = totalLenghtMs;
		this.animatable = animatable;
		load();
		if(animTilesLoadList.size() == 0){
			System.out.println("An animation was not loaded, it was empty...");
			return;
		}
		animTiles = animTilesLoadList.toArray(new AnimationTile[animTilesLoadList.size()]);
		java.util.Arrays.sort(animTiles);
		loaded = true;
	}
	
	protected void addTile(Tile tile, int startTime){
		addTile(tile, startTime, 0, 0);
	}
	
	protected void addTile(Tile tile, int startTime, int xOffset, int yOffset){
		if(loaded){
			System.out.println("Could not add an animTile, because the animation is already loaded...");
			return;
		}
		animTilesLoadList.add(new AnimationTile(tile, startTime, xOffset, yOffset));
	}
	
	public void start(){
		if(!loaded){
			System.out.println("Could not start an animation, because its not loaded yet... (" + this + ")");
			return;
		}
		currentTile = animTiles[0];
		running = true;
	}
	
	public void stop(){
		if(!running){
			return;
		}
		currentRuntime = 0;
		currentTileIndex = 0;
		running = false;
		animatable.onAnimationFinish(this);
	}
	
	protected abstract void load();
	
	public void update(int timeSinceLastUpdateMs){
		if(!running){
			return;
		}
		currentRuntime += timeSinceLastUpdateMs;
		if(currentRuntime >= totalLengthMs){
			stop();
			return;
		}
		if(currentTileIndex + 1 == animTiles.length){
			return;
		}
		AnimationTile next = animTiles[currentTileIndex + 1];
		if(currentRuntime >= next.startingTimeMs){
			currentTile = next;
			currentTileIndex++;
		}
	}
	
	public void render(Screen screen, int xPos, int yPos){
		if(currentTile == null){
			System.out.println("Current animTile == null");
			return;
		}
		screen.renderTile(currentTile.getTile(), xPos + currentTile.getXOffset(), yPos + currentTile.getYOffset());
	}
	
	public int getTotalLenght(){
		return totalLengthMs;
	}
	
	public boolean isRunning(){
		return running;
	}
	
}
